<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('pais', 'PaisController@index');
Route::post('pais', 'PaisController@store');
Route::get('pais/{id}', 'PaisController@get');
Route::put('pais/{id}', 'PaisController@update');
Route::delete('pais/{id}', 'PaisController@destroy');

Route::get('provinciaEstado', 'ProvinciaEstadoController@index');
Route::post('provinciaEstado', 'ProvinciaEstadoController@store');
Route::get('provinciaEstado/{id}', 'ProvinciaEstadoController@get');
Route::put('provinciaEstado/{id}', 'ProvinciaEstadoController@update');
Route::delete('provinciaEstado/{id}', 'ProvinciaEstadoController@destroy');

Route::get('ciudadCondado', 'CiudadCondadoController@index');
Route::post('ciudadCondado', 'CiudadCondadoController@store');
Route::get('ciudadCondado/{id}', 'CiudadCondadoController@get');
Route::put('ciudadCondado/{id}', 'CiudadCondadoController@update');
Route::delete('ciudadCondado/{id}', 'CiudadCondadoController@destroy');

Route::get('municipioCiudad', 'MunicipioCiudadController@index');
Route::post('municipioCiudad', 'MunicipioCiudadController@store');
Route::get('municipioCiudad/{id}', 'MunicipioCiudadController@get');
Route::put('municipioCiudad/{id}', 'MunicipioCiudadController@update');
Route::delete('municipioCiudad/{id}', 'MunicipioCiudadController@destroy');

//extras
Route::get('estadosPorPais/{id}', 'PaisController@estadosPorPais');
Route::post('paisByName', 'PaisController@paisName');
Route::post('municipioByName', 'MunicipioCiudadController@municipioName');
Route::post('ciudadCondadoByName', 'CiudadCondadoController@ciudadCondadoName');
Route::post('provinciaEstadoByName', 'ProvinciaEstadoController@provinciaEstadoName');

Route::get('getHijosPais/{id}', 'PaisController@getHijos');
Route::get('getHijosProvincia/{id}', 'ProvinciaEstadoController@getHijos');