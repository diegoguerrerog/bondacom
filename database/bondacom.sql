-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2018 a las 19:18:25
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bondacom`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad_condado`
--

CREATE TABLE `ciudad_condado` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_provincia_estado` int(11) NOT NULL,
  `municipio_ciudad` tinyint(1) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad_condado`
--

INSERT INTO `ciudad_condado` (`id`, `name`, `id_provincia_estado`, `municipio_ciudad`, `visible`) VALUES
(1, 'ciudad1', 2, 1, 0),
(2, 'ciudad2', 3, 1, 0),
(3, 'ciudad3', 3, 0, 1),
(4, 'ciudad4', 3, 0, 1),
(5, 'ciudad5', 2, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio_ciudad`
--

CREATE TABLE `municipio_ciudad` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_ciudad_condado` int(11) NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipio_ciudad`
--

INSERT INTO `municipio_ciudad` (`id`, `name`, `id_ciudad_condado`, `visible`) VALUES
(1, 'muni1', 2, 0),
(2, 'muni2', 1, 1),
(3, 'muni3', 4, 1),
(4, 'muni4', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `short_name` varchar(255) NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id`, `name`, `short_name`, `visible`) VALUES
(3, 'Argentina', 'ARG', 0),
(4, 'Venezuela', 'VE', 1),
(5, 'Chile', 'CL', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia_estado`
--

CREATE TABLE `provincia_estado` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_pais` int(11) NOT NULL,
  `visible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia_estado`
--

INSERT INTO `provincia_estado` (`id`, `name`, `id_pais`, `visible`) VALUES
(1, 'provincia1', 4, 0),
(2, 'provincia2', 4, 1),
(3, 'provincia3', 3, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad_condado`
--
ALTER TABLE `ciudad_condado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipio_ciudad`
--
ALTER TABLE `municipio_ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincia_estado`
--
ALTER TABLE `provincia_estado`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudad_condado`
--
ALTER TABLE `ciudad_condado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `municipio_ciudad`
--
ALTER TABLE `municipio_ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `provincia_estado`
--
ALTER TABLE `provincia_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
