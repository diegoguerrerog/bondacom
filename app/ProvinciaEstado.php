<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProvinciaEstado extends Model
{
    protected $table = 'provincia_estado';
    protected $fillable   = array('name' ,'id_pais');

    public $timestamps = false;
}
