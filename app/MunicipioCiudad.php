<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MunicipioCiudad extends Model
{
    protected $table = 'municipio_ciudad';
    protected $fillable   = array('name' ,'id_ciudad_condado');

    public $timestamps = false;
}
