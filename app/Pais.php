<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'pais';
    protected $fillable   = array('name' ,'short_name');

    public $timestamps = false;

    public function provinciasestados()
	{
	   return $this->hasMany('App\ProvinciaEstado','id_pais');
	}
}
