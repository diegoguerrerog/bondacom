<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CiudadCondado extends Model
{
    protected $table = 'ciudad_condado';
    protected $fillable   = array('name' ,'id_provincia_estado');

    public $timestamps = false;
}
