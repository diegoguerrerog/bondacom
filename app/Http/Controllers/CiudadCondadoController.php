<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ProvinciaEstado;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Pais;
use App\CiudadCondado;

class CiudadCondadoController extends Controller
{ 
    public function index(){
    	$ciudadCondado = CiudadCondado::all();
    	if (!$ciudadCondado->isEmpty()){
    		return $ciudadCondado;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function store(Request $request)
    {
    	try{

    		$rules = [
                'name' => 'required',
                'id_provincia_estado' => 'required'
            ];

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors' => $validator->errors()->all()
                ];
            }

            $provinciaEstado = ProvinciaEstado::find($request["id_provincia_estado"]);
            if ($provinciaEstado){
               $ciudadCondado = CiudadCondado::create($request->all());
               return $ciudadCondado;
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id de la provincia-estado"));
                return $result; 
            }

    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function get($id){
    	$ciudadCondado = CiudadCondado::find($id);
    	if ($ciudadCondado){
    		return $ciudadCondado;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function update(Request $request, $id)
    { 
    	try{
            $ciudadCondado = CiudadCondado::find($id);
            if ($ciudadCondado){
                $provinciaEstado = ProvinciaEstado::find($request["id_provincia_estado"]);
                if ($provinciaEstado){
                   DB::table('ciudad_condado')
                        ->where('id', $id)
                        ->update($request->all());
                        $result = json_encode(array("status"=>true, "message"=>['updated' => true]));
                    return $result; 
                }else{
                    $result = json_encode(array("status"=>false, "message"=>"No existe id de la provincia-estado"));
                    return $result; 
                }
               
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id de la provincia-estado"));
                return $result; 
            }
    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
            
    }

    public function destroy($id)
    {
        try{
            DB::table('ciudad_condado')
            ->where('id', $id)
            ->update(array("visible"=>0));
            $result = json_encode(array("status"=>true, "message"=>['deleted' => true]));
            return $result; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function ciudadCondadoName(Request $request)
    {
       
        if(!isset($request["name"]))
            return \Response::json(['error' => false,'message'=>'Necesita enviar el campo name'], 500);

        try{
                $result = DB::table('ciudad_condado')
                    ->where('name', $request["name"])->get();
                if($result){
                    return json_encode($result);
                }else{
                    return json_encode(array("status"=>false, "message"=>"No hay registros"));
                }
                return ; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['error' => false], 500);
        }
    }
}