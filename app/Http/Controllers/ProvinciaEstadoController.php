<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ProvinciaEstado;
use App\CiudadCondado;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Pais;
use App\MunicipioCiudad;
class ProvinciaEstadoController extends Controller
{ 
    public function index(){
    	$provinciaEstado = ProvinciaEstado::all();
    	if (!$provinciaEstado->isEmpty()){
    		return $provinciaEstado;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function store(Request $request)
    {
    	try{

    		$rules = [
                'name' => 'required',
                'id_pais' => 'required'
            ];

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors' => $validator->errors()->all()
                ];
            }

            $pais = Pais::find($request["id_pais"]);
            if ($pais){
               $provinciaEstado = ProvinciaEstado::create($request->all());
               return $provinciaEstado;
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id de pais"));
                return $result; 
            }

    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function get($id){
    	$provinciaEstado = ProvinciaEstado::find($id);
    	if ($provinciaEstado){
    		return $provinciaEstado;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function update(Request $request, $id)
    { 
    	try{
            $provinciaEstado = ProvinciaEstado::find($id);
            if ($provinciaEstado){
                $pais = Pais::find($request["id_pais"]);
                if ($pais){
                   DB::table('provincia_estado')
                        ->where('id', $id)
                        ->update($request->all());
                        $result = json_encode(array("status"=>true, "message"=>['updated' => true]));
                    return $result; 
                }else{
                    $result = json_encode(array("status"=>false, "message"=>"No existe id de pais"));
                    return $result; 
                }
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id de la provincia-estado"));
                return $result; 
            }
    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
            
    }

    public function destroy($id)
    {
        try{
            DB::table('provincia_estado')
            ->where('id', $id)
            ->update(array("visible"=>0));
            $result = json_encode(array("status"=>true, "message"=>['deleted' => true]));
            return $result; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function provinciaEstadoName(Request $request)
    {
       
        if(!isset($request["name"]))
            return \Response::json(['error' => false,'message'=>'Necesita enviar el campo name'], 500);

        try{
                $result = DB::table('provincia_estado')
                    ->where('name', $request["name"])->get();
                if($result){
                    return json_encode($result);
                }else{
                    return json_encode(array("status"=>false, "message"=>"No hay registros"));
                }
                return ; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['error' => false], 500);
        }
    }

    public function getHijos($id)
    {
        $hijos = array();
        $tmp = [];$tmp2 = [];$tmp3 = [];

            $provinciaEstado = ProvinciaEstado::find($id);
            if ($provinciaEstado){
            array_push($hijos, $provinciaEstado['name']);


           // for ($i=0; $i < count($provinciaEstado) ; $i++) { 
           //     array_push($tmp, $provinciaEstado[$i]['name']);
                $ciudadCondado = CiudadCondado::where('id_provincia_estado',$provinciaEstado['id'])->get();
                for ($j=0; $j < count($ciudadCondado) ; $j++) { 
                    array_push($tmp2, $ciudadCondado[$j]['name']);
                    if($ciudadCondado[$j]['municipio_ciudad']!=0){
                        $municipio = MunicipioCiudad::where('id_ciudad_condado',$ciudadCondado[$j]['id'])->get();
                        for ($k=0; $k < count($municipio) ; $k++) {
                            array_push($tmp3, $municipio[$k]['name']);
                        }
                        array_push($tmp2, $tmp3);
                    }
                        
                   
                }
                array_push($hijos, $tmp2);
           // }
               

            //return $provinciaEstado; 
            return $hijos;
        }else{
            return json_encode(array("status"=>false, "message"=>"No hay registros"));
        }

    }
}