<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\ProvinciaEstado;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Pais;
use App\CiudadCondado;
use App\MunicipioCiudad;

class MunicipioCiudadController extends Controller
{ 
    public function index(){
    	$municipioCiudad = MunicipioCiudad::all();

    	if (!$municipioCiudad->isEmpty()){
    		return $municipioCiudad;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function store(Request $request)
    {
    	try{

    		$rules = [
                'name' => 'required',
                'id_ciudad_condado' => 'required'
            ];

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors' => $validator->errors()->all()
                ];
            }

            $ciudadCondado = CiudadCondado::find($request["id_ciudad_condado"]);
            if ($ciudadCondado){
               $municipioCiudad = MunicipioCiudad::create($request->all());
               return $municipioCiudad;
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id del condado"));
                return $result; 
            }

    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function get($id){
    	$municipioCiudad = MunicipioCiudad::find($id);
    	if ($municipioCiudad){
    		return $municipioCiudad;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function update(Request $request, $id)
    { 
    	try{
            $municipioCiudad = MunicipioCiudad::find($id);
            if ($municipioCiudad){
                $ciudadCondado = CiudadCondado::find($request["id_ciudad_condado"]);
                if ($ciudadCondado){
                   DB::table('municipio_ciudad')
                        ->where('id', $id)
                        ->update($request->all());
                        $result = json_encode(array("status"=>true, "message"=>['updated' => true]));
                    return $result; 
                }else{
                    $result = json_encode(array("status"=>false, "message"=>"No existe id del condado"));
                    return $result; 
                }
               
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id del municipio"));
                return $result; 
            }
    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
            
    }

    public function destroy($id)
    {
        try{
            DB::table('municipio_ciudad')
            ->where('id', $id)
            ->update(array("visible"=>0));
            $result = json_encode(array("status"=>true, "message"=>['deleted' => true]));
            return $result; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function municipioName(Request $request)
    {
       
        if(!isset($request["name"]))
            return \Response::json(['error' => false,'message'=>'Necesita enviar el campo name'], 500);

        try{
                $result = DB::table('municipio_ciudad')
                    ->where('name', $request["name"])->get();
                if($result){
                    return json_encode($result);
                }else{
                    return json_encode(array("status"=>false, "message"=>"No hay registros"));
                }
                return ; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['error' => false], 500);
        }
    }
}