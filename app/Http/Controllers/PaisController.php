<?php

/**
 * @SWG\Swagger(
*   basePath="/bondacom/public/api",
*   @SWG\Info(
*     title="Test Bondacom",
*     version="1.0.0"
*   )
* )
*/

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Pais;
use Validator;
use Illuminate\Support\Facades\DB;
use App\ProvinciaEstado;
use App\CiudadCondado;
use App\MunicipioCiudad;
class PaisController extends Controller
{ 

    /**
     * @SWG\Get(
     *   path="/pais",
     *   summary="List de paises",
     *   operationId="index",
     *   tags={"Paises"},
     * security={
     *     {"passport": {}},
     *   },
     *  @SWG\Parameter(
     *         name="api-key",
     *         in="header",
     *         description="Ingrese el api-key",
     *         required=false,
     *         type="string"
     *     ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error")
     *
     * )
     *
     */
    public function index(){
    	$pais = Pais::all();
    	if (!$pais->isEmpty()){
    		return $pais;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    /**
    * @SWG\Post(
    *   path="/pais",
    *   tags={"Paises"},
    *   summary="Crear un pais",
    *   description="Create a pais",
    *   operationId="store",
    *   consumes={"application/x-www-form-urlencoded"},
    *   produces={"application/xml", "application/json"},
    *   
    *   @SWG\Parameter(
    *     name="name",
    *     in="formData",
    *     description="Nombre del pais",
    *     required=true,
    *     type="string"
    *   ),
    *   @SWG\Parameter(
    *         description="Abreviatura del pais",
    *         in="formData",
    *         name="short_name",
    *         required=true,
    *         type="string"
    *     ),
     *  @SWG\Parameter(
     *         name="api-key",
     *         in="header",
     *         description="Ingrese el api-key",
     *         required=false,
     *         type="string"
     *     ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error")
    *  
    * )
    */

    public function store(Request $request)
    {
    	try{

    		$rules = [
                'name' => 'required',
                'short_name' => 'required'
            ];

            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors' => $validator->errors()->all()
                ];
            }

    		$pais = Pais::create($request->all());
       		return $pais;
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    /**
     * @SWG\Get(
     *   path="/pais/{id}",
     *   summary="Muestra pais por id",
     *   operationId="get",
     *   tags={"Paises"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id, primary key de paises",
     *     required=true,
     *     type="string"
     *   ),
     *  @SWG\Parameter(
     *         name="api-key",
     *         in="header",
     *         description="Ingrese el api-key",
     *         required=false,
     *         type="string"
     *     ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */

    public function get($id){
    	$pais = Pais::find($id);
    	if ($pais){
    		return $pais;
    	}else{
    		$result = json_encode(array("status"=>false, "message"=>"No hay registros"));
        	return $result; 
    	}
    }

    public function update(Request $request, $id)
    { 
    	try{
    		DB::table('pais')
            ->where('id', $id)
            ->update($request->all());
            $result = json_encode(array("status"=>true, "message"=>['updated' => true]));
        	return $result; 
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['update' => false], 500);
        }
            
    }

    /**
     * @SWG\Delete(
     *     path="/pais/{id}",
     *     summary="Borrar un pais (visible 0)",
     *     description="",
     *     operationId="destroy",
     *     consumes={"application/xml", "application/json", "multipart/form-data", "application/x-www-form-urlencoded"},
     *     produces={"application/xml", "application/json"},
     *     tags={"Paises"},
     *     @SWG\Parameter(
     *         description="Ingrese id del pais",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string",
     *         
     *     ),
     *  @SWG\Parameter(
     *         name="api-key",
     *         in="header",
     *         description="Ingrese el api-key",
     *         required=false,
     *         type="string"
     *     ), 
     *     @SWG\Response(response=200, description="successful operation"),
     *     @SWG\Response(response=500, description="internal server error")
     *     
     * )
     */

    public function destroy($id)
    {
    	try{
            $pais = Pais::find($id);
            if ($pais){
               DB::table('pais')
                ->where('id', $id)
                ->update(array("visible"=>0));
                $result = json_encode(array("status"=>true, "message"=>['deleted' => true]));
                return $result; 
            }else{
                $result = json_encode(array("status"=>false, "message"=>"No existe id de pais"));
                return $result; 
            }
    		
    	}catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['created' => false], 500);
        }
    }

    public function estadosPorPais($id){
    	$pais = Pais::find($id);
		$provincias= $pais->provinciasestados;
		return $provincias;
    }

    public function paisName(Request $request)
    {
       
        if(!isset($request["name"]))
            return \Response::json(['error' => false,'message'=>'Necesita enviar el campo name'], 500);

        try{
                $result = DB::table('pais')
                    ->where('name', $request["name"])->first();
                if($result){
                    return json_encode($result);
                }else{
                    return json_encode(array("status"=>false, "message"=>"No hay registros"));
                }
                return ; 
        }catch (Exception $e) {
            \Log::info('Hubo un error, intente de nuevo: ' . $e);
            return \Response::json(['error' => false], 500);
        }
    }

    public function getHijos($id)
    {
        $hijos = array();
        $tmp = [];$tmp2 = [];$tmp3 = [];

        $pais = Pais::find($id);
        if ($pais){
            array_push($hijos, $pais['name']);
            $provinciaEstado = ProvinciaEstado::where('id_pais',$id)->get();
            for ($i=0; $i < count($provinciaEstado) ; $i++) { 
                array_push($tmp, $provinciaEstado[$i]['name']);
                $ciudadCondado = CiudadCondado::where('id_provincia_estado',$provinciaEstado[$i]['id'])->get();
                for ($j=0; $j < count($ciudadCondado) ; $j++) { 
                    array_push($tmp2, $ciudadCondado[$j]['name']);
                    if($ciudadCondado[$j]['municipio_ciudad']!=0){
                        $municipio = MunicipioCiudad::where('id_ciudad_condado',$ciudadCondado[$j]['id'])->get();
                        for ($k=0; $k < count($municipio) ; $k++) {
                            array_push($tmp3, $municipio[$k]['name']);
                        }
                        array_push($tmp2, $tmp3);
                    }
                        
                   
                }
                array_push($tmp, $tmp2);
            }
                array_push($hijos, $tmp);

            //return $provinciaEstado; 
            return $hijos;
        }else{
            return json_encode(array("status"=>false, "message"=>"No hay registros"));
        }

    }
}